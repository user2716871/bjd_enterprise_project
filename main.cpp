#include <iostream>
#include <string>

#include "my_math.h"
#include "model.h"

using namespace std;

int main() {

	Material material{"Iron"s, 7874 }; // kg / m**3

	Gas gas{ "Carbon monoxide"s, 1.4 }; 


	double outer_radius = 12.000; // meters
	double thickness = 0.02; // meters
	SphericalShell spherical_shell = SphericalShell(outer_radius, thickness);

	double pressure = 900'000; // Pascals
	double distance_to_object = 50.0; // meters
	StorageTank storage_tank(spherical_shell, material, gas, pressure, distance_to_object);

	storage_tank.CalcAll();


}