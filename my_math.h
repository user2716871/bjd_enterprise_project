#pragma once

#include <cmath>

const double PI = 3.14159265359;

double Sqr(double x);

double Cube(double x);

double CircleArea(double r);

double SphereVolume(double r);

double CylinderVolume(double r, double h);
