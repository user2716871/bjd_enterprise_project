#include "my_math.h"

double Sqr(double x) {
	return x * x;
}

double Cube(double x) {
	return Sqr(x) * x;
}

double CircleArea(double r) {
	return PI * Sqr(r);
}

double SphereVolume(double r) {
	return 4.0 / 3.0 * PI * Cube(r);
}

double CylinderVolume(double r, double h) {
	return CircleArea(r) * h;
}
